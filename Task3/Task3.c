#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

int main() {
	int n, a[10000], p = 0;
	printf("Enter array size n (0<n<10000) and its elements: ");
	scanf("%d", &n);
	for (int i = 0; i < n; i++) {
		scanf("%d", &a[i]);
	}
	int t = a[n-1];
	for (int i = n - 2; i >= 0; i--) {
		a[i+1] = a[i];
	}
	a[0] = t;
	for (int i = 0; i < n; i++) {
		printf("%d ", a[i]);
	}
	return 0;
}