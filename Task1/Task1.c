#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

int main() {
	int n, m[10000];
	printf("Enter array size (0<n<10000) and its elements\n");
	scanf("%d", &n);
	for (int i = 0; i < n; i++) {
		scanf("%d", &m[i]);
	}
	int s=-1000000000;
	for (int i = 1; i < n-1; i++) {
		s = m[i] < m[i + 1] && m[i] < m[i - 1] && m[i] > s ? m[i] : s;
	}
	if(s == -1000000000)
		printf("No local minimums found in array");
	else
		printf("Highest local minimum: %d", s);
	return 0;
}