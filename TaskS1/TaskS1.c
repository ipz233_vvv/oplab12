#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

int main() {
	int n=11, a[11], p[11], m[11], mm=0,pp=0,mc=0;
	printf("Enter 11 elements of array\n");
	for (int i = 0; i < n; i++) {
		scanf("%d", &a[i]);
		if (a[i] > 0) {
			p[pp] = a[i];
			pp++;
		}
		else {
			m[mc] = a[i];
			mc++;
			mm += a[i] == 0 ? 0 : 1;
		}
	}
	for (int i = 0; i < n; i++) {
		a[i] = i < pp ? p[i] : m[i - pp];
		printf("%d ", a[i]);
	}
	printf("\nArray contains:\n%d negative elements\n%d positive elements\n%d zeros", mm,pp,n-mm-pp);
	return 0;
}