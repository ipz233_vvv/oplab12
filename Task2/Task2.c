#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

int main() {
	int n, a[10000],b[10000], p = 0;
	printf("Enter array size n (0<n<10000) and its elements\n");
	scanf("%d", &n);
	for (int i = 0; i < n; i++) {
		scanf("%d", &a[i]);
	}
	for (int i = 0; i < n; i++) {
		if (a[i] % 2 == 0) {
			b[p] = a[i];
			p++;
			printf("%d ", b[p-1]);
		}
	}
	printf("\nSize of output array: %d", p);
	return 0;
}