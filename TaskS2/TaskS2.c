#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>

int main() {
	int n=14, f[14],f2[14], pp[14], mm[14], p = 0, m = 0;

	printf("Enter 14 elements of array\n");

	for (int i = 0; i < n; i++) {
		scanf("%d", &f[i]);
		if (f[i] > f[0]) {
			pp[p] = f[i];
			p++;
		}
		else {
			mm[m] = f[i];
			m++;
		}
	}
	printf(  "   Start array: ");
	for (int i = 0; i < n; i++) {
		printf("%d ", f[i]);
	}
	printf("\nModified array: ");
	for (int i = 0; i < n; i++) {
		f2[i] = i < p ? pp[i] : mm[i-p];
		printf("%d ", f2[i]);
	}

	return 0;
}
